void main() {
  var GetApps = new PrintApps();

  //GetApps.DisplayApps();
}

class PrintApps {
  // "2012:FNB Banking App";
  // "2013:SnapScan";
  // "2014:Live-inspect";
  // "2015:WumDrop:Simon Hartley and Roy Borole";
  // "2016:Domestly";
  // "2017:Shyft";
  // "2018:Khula!":Karidas Tshintsholo and Matthew Piper;
  // "2019:Naked Insurance";
  // "2020:Checkers Sixty60";
  // "2021:Ambani Africa";

  PrintApps() {
    DisplayApps();
  }

  List appName = [
    "FNB Banking",
    "SnapScan",
    "Live-inspect",
    "WumDrop",
    "Domestly",
    "Shyft",
    "Khula!",
    "Naked Insurance",
    "Checkers Sixty60",
    "Ambani Africa"
  ];
  List appCategory = [
    "Best Consumer Android App",
    "Best HTML5 App",
    "Best Android Entreprise App",
    "Best Enterprise App",
    "Best Consumer App",
    "Best Financial Solution",
    "Best Agriculture Solution",
    "Best Financial Solution",
    "Best Enterprise Solution",
    "Best Educational Solution"
  ];
  List appDeveloper = [
    "FNB",
    "FireID",
    "LightStone Auto/CustomApp",
    // "Benjamin Claassen/Muneeb Samuels",
    "B.Claassen/M.Samuels",
    // "Thatoyoana Marumo/Berno Porgieters",
    "T.Marumo/B.Porgieters",
    "Standard Bank",
    "K.Tshintsholo and M.Piper",
    "Naked Insurance",
    "Checkers",
    "Ambani Africa"
  ];
  List appYear = [
    "2012",
    "2013",
    "2014",
    "2015",
    "2016",
    "2017",
    "2018",
    "2019",
    "2020",
    "2021"
  ];

  String name = "";
  String category = "";
  String developer = "";
  String year = "";

  void DisplayApps() {
    for (int i = 0; i < appName.length; i++) {
      name = appName[i];
      category = appCategory[i];
      developer = appDeveloper[i];
      year = appYear[i];

      print(year.padRight(5, ' ') +
          " " +
          MakeCapital(name).padRight(18, '-') +
          " " +
          developer.padRight(27, '-') +
          " " +
          category);
    }
  }

  String MakeCapital(String text) {
    String input;

    input = text;

    return input.toUpperCase();
  }
}
