import "dart:collection";

void main() {
  Map overallWinners = new Map();

  overallWinners['2012'] = "FNB Banking App";
  overallWinners['2013'] = "SnapScan";
  overallWinners['2014'] = "Live-inspect";
  overallWinners['2015'] = "WumDrop";
  overallWinners['2016'] = "Domestly";
  overallWinners['2017'] = "SHYFT";
  overallWinners['2018'] = "khula!";
  overallWinners['2019'] = "Naked Insurance";
  overallWinners['2020'] = "Checkers Sixty60";
  overallWinners['2021'] = "Ambani Africa";

  //var unsorted = {'A': 3, 'B': 1, 'C': 2};
  final sorted = SplayTreeMap.from(overallWinners,
      (key1, key2) => overallWinners[key1].compareTo(overallWinners[key2]));

  // var overallWinners = <String>[];

  // overallWinners.add("FNB App");
  // overallWinners.add("SnapScan");
  // overallWinners.add("Live-inspect");
  // overallWinners.add("Wumdrop");
  // overallWinners.add("Domestly");
  // overallWinners.add("SHYFT");
  // overallWinners.add("khula!");
  // overallWinners.add("Naked Insurance");
  // overallWinners.add("Checkers Sixty60");
  // overallWinners.add("Ambani Africa");

  // overallWinners.sort();

  // for (int i = 0; i < overallWinners.length; i++) {
  //   print((sorted[i]));
  // }

  //Sorted
  sorted.forEach((k, v) => print("Year : $k, Overall Winner : $v"));

  //Winners of 2017 & 2018
  print(sorted['2017']);
  print(sorted['2018']);

  //Elements in array
  print(sorted.length);
}
